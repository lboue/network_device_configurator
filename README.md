What I have in mind is to build a sort of automation tool where the config for a device is changed only via YAML. Taking Juniper as vendor fro a POC, I have tried to create a folder structure similar to Openconfig hierarchy (following Juniper structure), on each folder there is a YAML file for specific confg section (i.e. DNS, NTP, TACACS, etc). Variables can be also written in ```all.yml``` and reused for other devices. However more specific YAML variables take priority

This idea wants to move the concept of a network device configuration from classic config file saved in FTP to Git were it can go through the classic git code review process

This example is based on JUNOS device (SRX300 in this specific case)

```
.
└── firewalls
    ├── all.yaml
    └── srx00.sw11.lab
        ├── config.j2
        ├── firewall
        │   ├── COPP.yaml
        │   └── WAN.yaml
        ├── interfaces.yaml
        ├── security
        │   ├── nat
        │   │   └── destination.yaml
        │   ├── policies.yaml
        │   └── zones.yaml
        ├── services
        │   └── rpm.yaml
        ├── snmp
        │   └── snmp.yaml
        ├── system
        │   ├── host-name.yaml
        │   ├── login
        │   │   ├── services
        │   │   └── user.yaml
        │   ├── name-server.yaml
        │   ├── ntp.yaml
        │   ├── services.yaml
        │   └── tacplus-server.yaml
        └── vlans.yaml
```

Based on jinja2, a template is rendered using the YAML file variables.
Once the config file is rendered, we use NAPALM to perform config replace.

Using this process CLI configuration is not longer allowed as every change, if is not reflected in YAML file, it will be overwritten  CI/CD pipeline will be the next step

```
federico@federico:~/git/myowngitlab/networkAutomation/juniperPipeline $ python3 render.py juniper/firewalls/srx00.sw11.lab/config.j2
Loading config file
Opening...

Diff:
[edit firewall filter COPP term ALLOW-NTP from source-address]
+      192.168.1.12/32;
-      192.168.1.50/32;
[edit firewall filter COPP]
-    term ALLOW-OSPF {
-        from {
-            source-address {
-                192.168.0.0/16;
-            }
-        }
-        then {
-            count COPP-ALLOW-OSPF;
-            accept;
-        }
-    }

Would you like to commit these changes? [yN]: y
Committing ...
Done.
```

CI/CD pipeline has to be built.
